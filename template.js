import { forms } from "./templates/forms.js";
import { htmlize } from "./util.js";

const PROJECTS = $('#project');

export const init = () => {
  const dropdown = $("#typeDropdown");
  // Add values to dropdown
  forms.forEach((form) => {
    let option = $('<option></option>').val(form.type).text(form.type);
    console.log(form.default_project);
    option.data('default_project', form.default_project)
      .data('project_restricted', form.project_restricted);
    dropdown.append(option);
  });
  // Create HTML
  forms.forEach((form) => createForm(form, 'form'));

  const hide = (clazz) => {
    $('.' + clazz).hide().attr('required', false);
  }

  const show = (clazz) => {
    $('.' + clazz).show().attr('required', function() {
      return $(this).data('required');
    });
  }

  dropdown.change(function (e) {
    // Hide all form fields
    forms.forEach((form) => hide(form.type));
    // Show only fields with the selected class
    if (this.value) {
      show(this.value);
    }
    let selected = $(`option[value=${this.value}]`, this);
    PROJECTS.val(selected.data('default_project'));
    // Enable all options
    $('option', PROJECTS).prop('disabled', false);
    // Disable all but restricted
    if (selected.data('project_restricted')) {
      $('option', PROJECTS).prop('disabled', true);
      selected.prop('disabled', false);
    }
  });
  dropdown.trigger('change');
}

const getForm = (type) => {
  return forms.filter((form) => form.type === type)[0];
}

// Get the input element for a field
const getInput = (field) => {
  switch(field.type) {
    case 'textinput': return $('<input type="text">');
    case 'textarea': return $('<textarea>').attr('rows', field.rows);
    default: throw Error();
  }
}

// Create Form fields from JSON template and add it to the element with formId
export const createForm = (formTemplate, formId) => {
  const type = formTemplate.type;
  formTemplate.fields.forEach((field) => {
    const label = $('<label>').attr('for', field.id).text(field.label);
    const input = getInput(field);
    input.addClass('form-control')
    .addClass(type)
    .attr('id', makeId(type, field.id))
    .attr('placeholder', field.placeholder)
    .data('required', field.required);
    const formField = $('<div class="form-group">').addClass(type)
    formField.append(label);
    formField.append(input);
    formField.insertBefore('#' + formId + ' button');
  });
}

// Create Text from the template of the form of the given type
export const createText = (type) => {
  // Get the relevant form
  const form = getForm(type);
  if(!form) {
    return '';
  }
  // Fill fields object with the value of the corresponding form elements
  let fields = {};
  form.fields.forEach((field) => {
    const id = makeId(type, field.id);
    fields[field.id] = $('#' + id).val();
  });
  return htmlize(form.template(fields));
};

// Combine type and field names to prevent collisions (e.g. of title)
const makeId = (type, field) => {
  return `${type}_${field}`;
}

export const makeTitle = (type) => {
  const id = makeId(type, 'title');
  return $('#' + id).val();
}
