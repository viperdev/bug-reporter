export const htmlize = function(input) {
  return input.replace(/(\r\n|\n|\r)/gm,"</br>");
};
