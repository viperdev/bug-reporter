import * as productive from "./productive.js";
import * as template from "./template.js";

let _debug = false;
let debugActions = {};

$('#show-auth').click(function (e) {
  $('#authenticator').show();
});

const populateProjects = async function (org, token) {
  const resJson = await productive.call(org, token, '/projects');
  var projects = $('#project');
  $.each(resJson.data, function() {
    if (this.attributes.archived_at === null) {
      let option = $('<option></option>').text(this.attributes.name).val(this.id);
      option.attr('title', this.id);
      projects.append(option);
    };
  });
  debugActions['populate'] = () => $('#project').find('option').each(function() {
    $(this).text('[' + this.value + '] ' + $(this).text());
  });
  $("#typeDropdown").trigger('change');
};

const getCommentText = function() {
  return template.createText($('#typeDropdown').val());
};
const getTitle = function() {
  return template.makeTitle($('#typeDropdown').val());
};

var token = '';
var org = '';

$(function() {
  token = Cookies.get('X-Auth-Token');
  org = Cookies.get('X-Organization-Id');
  if (token && org) {
    localStorage.setItem('X-Auth-Token', token);
    localStorage.setItem('X-Organization-Id', org);
  } else {
    token = localStorage.getItem('X-Auth-Token');
    org = localStorage.getItem('X-Organization-Id');
  }
  if (token && org) {
    $('#authenticator').hide();
    populateProjects(org, token);
  }

  // Initialise the template
  template.init();
});

$('#token-cancel').click(function(e) {
  $('#authenticator').hide();
});

$('#token-submit').click(function (e) {
  if ($('#token').val() == '') {
    alert('Please set a token :)');
  } else if ($('#org').val() == '') {
    alert('Please set an organization ID :)');
  } else {
    token = $('#token').val();
    org = $('#org').val();
    localStorage.setItem('X-Auth-Token', token);
    localStorage.setItem('X-Organization-Id', org);
    Cookies.set('X-Auth-Token', token, {expires: 90});
    Cookies.set('X-Organization-Id', org, {expires: 90});
    $('#authenticator').hide();
    populateProjects(org, token);
  }
});

$( '#form' ).submit(function( e ) {
  e.preventDefault();
  e.stopPropagation();
  if (form.checkValidity() === true) {
    const project = $('#project').children("option:selected").val();
    const commentText = getCommentText();
    const title = getTitle();
    // Only log task in debug mode
    if (_debug) {
      console.log('Project', project);
      console.log('title', title);
      console.log('commentText', commentText);
      alert('No task was created because you are in debug mode. Refresh the page to disable debug mode.');
    } else {
      productive.submitTask(org, token, project, title, commentText);
      $( '#success' ).toast('show');
    }
    $( '#form' ).trigger('reset');
    $( '.needs-validation' ).removeClass('was-validated');
  } else {
    $( '.needs-validation' ).addClass('was-validated');
    $( '#failure' ).toast('show');
  }
});
$( '#success' ).toast({delay: 2000});
$( '#failure' ).toast({delay: 2000});

const SEQUENCE = [38, 38, 40, 40, 37, 39, 37, 39, 66, 65]; // Konami code
let cur_seq = [];
$(document).keyup((e) => {
  cur_seq.push(e.keyCode);
  for (let i = 0; i < cur_seq.length; ++i) {
    if (cur_seq[i] != SEQUENCE[i]) {
      cur_seq = [e.keyCode];
      break;
    }
  }
  if (cur_seq.length === SEQUENCE.length && 
    cur_seq.every((v, i) => v === SEQUENCE[i])) {
      console.log('Enabled debug mode');
      _debug = true;
      for (let action in debugActions) {
        debugActions[action]();
      }
  }
})
