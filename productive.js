const API = 'https://api.productive.io/api/v2';

export const call = async function(org, token, endpoint, method = 'GET', body = undefined) {
  console.log('call', org, token, endpoint, method, body);
  const response = await fetch(API + endpoint + '?page[size]=200', {
    method: method,
    headers: {
      'X-Auth-Token': token,
      'X-Organization-Id': org,
      'Content-Type': 'application/vnd.api+json',
    },
    body: JSON.stringify(body)
  });

  return await response.json();
};

/*
 * Submits the task, adds the comment, links it properly and pins the comment.
 */
export async function submitTask(org, token, project, title, commentText) {
  console.log('submit Task', org, token, project, title, commentText);
  const listId = await getFirstListId(org, token, project);
  const comment = await createProductiveComment(org, token, commentText);
  const task = await createTask(org,
                                token,
                                title,
                                project,
                                listId,
                                comment.data.id);
  pinComment(org, token, comment.data.id);
};

const getFirstListId = async function(org, token, project_id) {
  const taskLists = await call(org, token, '/task_lists?filter[project_id]=' + project_id);
  return taskLists.data[0].id;  // First list is the default go to list
};

const createTask = async function(org, token, title, projectId, taskListId, commentId) {
  return await call(org, token, '/tasks', 'POST', {
    "data": {
      "type": "tasks",
      "attributes": {
        "title": title
      },
      "relationships": {
        "project": {
          "data": {
            "type": "projects",
            "id": projectId
          }
        },
        "task_list": {
          "data": {
            "type": "task_lists",
            "id": taskListId
          }
        },
        "last_comment": {
          "data": {
            "type": "comments",
            "id": commentId,
          }
        }
      }
    }
  });
};

async function pinComment(org, token, commentId) {
  return await call(org, token, `/comments/${commentId}/pin`, 'PATCH')
}

const createProductiveComment = async function(org, token, commentBody) {
  const comment = {
    "data": {
      "type": "comments",
      "attributes": {
        "body": commentBody,
        "commentable_type": "task"
      }
    }
  };
  return await call(org, token, '/comments', 'POST', comment);
};
