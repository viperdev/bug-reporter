export const forms = [
  {
    "type": "Bug", // Shown in dropdown
    "default_project": 23537, // Preselected project
    "project_restricted": false, // If true, only allow default project
    "fields": [
      {
        "id": "title",
        "type": "textinput",
        "label": "Task Title",
        "placeholder": "Title - keep it short and precise!",
        "required": true
      },
      {
        "id": "steps",
        "type": "textarea",
        "rows": "4",
        "label": "Steps to Reproduce",
        "placeholder": "1. \n2. \n3.",
        "required": true
      },
      {
        "id": "actual",
        "type": "textarea",
        "rows": "2",
        "label": "Actual Results",
        "placeholder": "- \n-",
        "required": true
      },
      {
        "id": "expected",
        "type": "textarea",
        "rows": "2",
        "label": "Expected Results",
        "placeholder": "- \n-",
        "required": true
      },
      {
        "id": "other",
        "type": "textarea",
        "rows": "2",
        "label": "Additional Information",
        "placeholder": "- \n-",
        "required": false
      }
    ],
    "template": (fields) => `<div>
<strong>Steps to Reproduce:</strong>\n\n${fields.steps}\n\n
<strong>Actual Result:</strong>\n\n${fields.actual}\n\n
<strong>Expected Result:</strong>\n\n${fields.expected}\n\n
<strong>Additional Information:</strong>\n\n${fields.other}\n\n
</div>`
  },
  {
    "type": "Feature",
    "default_project": 23537, // Bug Reporter
    "project_restricted": false,
    "fields": [
      {
        "id": "title",
        "type": "textinput",
        "label": "Task Title",
        "placeholder": "Title - keep it short and precise!",
        "required": true
      },
      {
        "id": "user",
        "type": "textinput",
        "label": "As a...",
        "placeholder": "Person who wants to achieve something",
        "required": true
      },
      {
        "id": "purpose",
        "type": "textinput",
        "label": "I want to...",
        "placeholder": "Achieve something",
        "required": true
      },
      {
        "id": "motivation",
        "type": "textinput",
        "label": "So that...",
        "placeholder": "My motivation (is fulfilled)",
        "required": true
      },
      {
        "id": "testing",
        "type": "textarea",
        "rows": "2",
        "label": "Testing Criteria (Keypoints)",
        "placeholder": "- \n-",
        "required": true
      },
      {
        "id": "other",
        "type": "textarea",
        "rows": "2",
        "label": "Additional Information",
        "placeholder": "",
        "required": false
      }
    ],
    "template": (fields) => `<div>
As a <strong>${fields.user}</strong> I want to <strong>${fields.purpose}</strong>
 so that <strong>${fields.motivation}</strong>.\n\n
<strong>Testing Criteria:</strong>\n\n${fields.testing}\n\n
<strong>Additional Information:</strong>\n\n${fields.other}\n\n
</div>`
  },
  {
    "type": "ClientBrief",
    "default_project": 20324, // Company Development
    "project_restricted": true,
    "fields": [
      {
        "id": "title",
        "type": "textinput",
        "label": "One Line Status Summary",
        "placeholder": "'Yujet: Budget: ok, Timeline: 1 wk delayed, Blockers: few'",
        "required": true
      },
      {
        "id": "budget",
        "type": "textinput",
        "label": "Budget Available (in EUR)",
        "placeholder": "E.g. '25000'",
        "required": true
      },
      {
        "id": "budget_burnt",
        "type": "textinput",
        "label": "Budget Burnt (in EUR)",
        "placeholder": "E.g. '13500'",
        "required": true
      },
      {
        "id": "total_sprints",
        "type": "textinput",
        "label": "Total Estimated Sprints",
        "placeholder": "E.g. '6'",
        "required": true
      },
      {
        "id": "complete_sprints",
        "type": "textinput",
        "label": "Completed Sprints",
        "placeholder": "E.g. '3'",
        "required": true
      },
      {
        "id": "change_requests",
        "type": "textarea",
        "rows": "2",
        "label": "Major Change Requests",
        "placeholder": "- List Change Requests (including additional budget)\n- Those have to be approved by the client.",
        "required": false
      },
      {
        "id": "eta_internal",
        "type": "textinput",
        "label": "Estimated Project Completion Date (Internal)",
        "placeholder": "When do you estimate the project to finish, assuming some things go wrong?",
        "required": true
      },
      {
        "id": "eta_external",
        "type": "textinput",
        "label": "Estimated Project Completion Date (External)",
        "placeholder": "Usually at least two weeks after the internal completion date.",
        "required": true
      },
      {
        "id": "last_sprint_goal",
        "type": "textinput",
        "label": "Last Sprint Goal Summary",
        "placeholder": "Complete ... .",
        "required": true
      },
      {
        "id": "last_sprint_features",
        "type": "textarea",
        "rows": "3",
        "label": "Features That Have Been Delivered This Sprint",
        "placeholder": "- Describe main features\n- Link productive tasks in parentheses",
        "required": true
      },
      {
        "id": "last_sprint_bugfixes",
        "type": "textarea",
        "rows": "3",
        "label": "Major Bugfixes Last Sprint",
        "placeholder": "- Describe major bugfixes\n- Link productive tasks in parentheses (task link here :))",
        "required": false
      },
      {
        "id": "next_sprint_goal",
        "type": "textinput",
        "label": "Upcoming Sprint Goal Summary",
        "placeholder": "Complete ... .",
        "required": true
      },
      {
        "id": "needs_approval",
        "type": "textarea",
        "rows": "3",
        "label": "Decisions Requiring Approval",
        "placeholder": "- Describe main decisions the client needs to sign off so we can proceed\n- If needed make aware of budget implications",
        "required": false
      },
      {
        "id": "blockers",
        "type": "textarea",
        "rows": "3",
        "label": "Blockers/Red Flags",
        "placeholder": "- E.g. communication issues with the client or a party related to the client\n- E.g. if we're waiting for something",
        "required": false
      },
    ],
    "template": (fields) => `<h1>Client Brief</h1>
<div><strong>Budget:</strong> ${fields.budget_burnt}€ used / ${fields.budget}€ available</div>
<div><strong>Sprints:</strong> ${fields.complete_sprints} done / ${fields.total_sprints} expected total</div>
<div><strong>Major Change Requests:</strong>\n${fields.change_requests}</div>
<div><strong>Estimated Completion Date:</strong> ${fields.eta_external}</div>

<h2>Sprint Overview</h2>
<div><strong>Last Sprint Goal:</strong> ${fields.last_sprint_goal}</div>
<div><strong>Delivered Features:</strong>\n${fields.last_sprint_features}</div>
<div><strong>Delivered Major Bugfixes:</strong>\n${fields.last_sprint_bugfixes}</div>
<div><strong>Upcoming Sprint Goal:</strong> ${fields.next_sprint_goal}</div>
<div><strong>Decisions Requiring Approval:</strong>\n${fields.needs_approval}</div>
<div><strong>Blockers/Red Flags:</strong>\n${fields.blockers}</div>

<h1>Additional Internal Info</h1>
<div><strong>Estimated Completion Date:</strong> ${fields.eta_internal} (external: ${fields.eta_external})</div>`
  }
]
